# Autor:"Martha Cango"
# Email:"martha.cango@unl.edu.ec"


def double_char(palabra):
    lista = []
    for simbolo in palabra:
        lista.append(simbolo + simbolo)
    return ("".join(lista))


if __name__ == "__main__":
    usuario = input("Ingrese una palabra: ")
    print(double_char(usuario))