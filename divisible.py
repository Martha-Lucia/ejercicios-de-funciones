#Autor:"Martha Cango"
#Email:"martha.cango@unl.edu.ec"


def divisible_by(number, divisor):
    if number % divisor == 0:
        return True
    else:
        return False


if __name__ == "__main__":
    number = int(input("Ingrese un número que desee: "))
    divisor = int(input("Ingrese el divisor que desee: "))
    print(divisible_by(number, divisor))