# Autor:"Martha Cango"
# Email:"martha.cango@unl.edu.ec"


def count_e(name):
    palabra_minuscula = name.count("e")
    palabra_mayuscula = name.count("E")
    suma = palabra_minuscula + palabra_mayuscula
    return suma


if __name__ == "__main__":
    usuario = input("Ingrese un palabra que desee: ")
    print(count_e(usuario))
